import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { ClinicsService } from './clinics.service';

@Component({
    selector: 'app-backend-clinics',
    templateUrl: './clinics.component.html',
    styleUrls: ['./clinics.component.scss']
})
export class ClinicsComponent {

    clinics: any;
    displayForm: boolean = false;

    clinicName :string = '';
    clinicActive :boolean = true;

    clinicIdEdit :number = 0;
    clinicNameEdit :string = '';
    clinicActiveEdit :boolean = true;

    isAdd :boolean = false;
    isEdit :boolean = false;

    clinicStatus = [
        {label: 'Active', value: true},
        {label: 'Inactive', value: false}
    ];

    constructor(
        private layoutService: LayoutService,
        private clinicsService: ClinicsService
    )
    {
    }

    async ngOnInit() {
        await this.getData();
    }

    // get Services data from API
    async getData() {
        const res:any  = await this.clinicsService.list();
        let clinics = [];
        clinics = res.data;
        console.log('clinics:',clinics);
        if(clinics.ok){
            this.clinics = clinics.results;
        } else {
            alert('Error loading clinics');
        }
    }

    // display form add data
    displayFormAdd() {
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;
        this.clinicName = '';
    }

    // display form edit data
    displayFormEdit(data: any) {
        this.isAdd = false;
        this.isEdit = true;
        this.clinicIdEdit = data.service_id;
        this.clinicNameEdit = data.service_name;
        this.clinicActiveEdit = data.is_active;
        this.displayForm = true;
    }

    // function save data
    async save() {
        let data = {
            service_name: this.clinicName
        };

        // save data
        await this.clinicsService.save(data);

        // close form
        this.displayForm = false;
        
        //refresh data
        await this.getData();
    }

    // function update data
    async update() {
        let id = this.clinicIdEdit;
        let body = {
            service_name: this.clinicNameEdit,
            is_active: this.clinicActiveEdit
        };

        // update data
        await this.clinicsService.update(id, body);

        // close form
        this.displayForm = false;

        // refresh data
        await this.getData();
    }

    // set is active = false
    async disable(id: number) {
        let body = {
            is_active: false
        };
        await this.clinicsService.update(id, body);

        // refresh data
        await this.getData();
    }
    
    // set is active = true
    async enable(id: number) {
        let body = {
            is_active: true
        };
        await this.clinicsService.update(id, body);

        // refresh data
        await this.getData();
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({behavior: 'smooth'});
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image = this.layoutService.config.colorScheme === 'dark' ? 'line-effect-dark.svg' : 'line-effect.svg'; 

        return {'background-image': 'url(' + path + image + ')'};
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }
}