import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class ClinicsService {
    // pathPrefix: any = `:40014`
    pathPrefixAuth: any = `:40001/services`;

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}${this.pathPrefixAuth}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

    async list() {
        const url = `/`;
        return this.axiosInstance.get(url);
    }

    async getById(id: any) {
        const url = `/` + id;
        return this.axiosInstance.get(url);
    }

    async save(data: any) {
        const url = `/`;
        return this.axiosInstance.post(url, data);
    }

    async update(id: any, data: any) {
        const url = `/` + id;
        return this.axiosInstance.put(url, data);
    }
}
