import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeComponent} from './components/home/home.component';
import { SlotsComponent } from './components/slots/slots.component';
import { UsersComponent } from './components/users/users.component';
import { RolesComponent } from './components/roles/roles.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ClinicsComponent } from './components/clinics/clinics.component';
import { PeriodsComponent } from './components/periods/periods.component';
import { ManagementsComponent } from './components/managements/managements.component';
import { ServicesComponent } from './components/services/services.component';
import { HospitalsComponent } from './components/hospitals/hospitals.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', data: { breadcrumb: 'Home' }, component: HomeComponent },
            { path: 'dashboard', data: { breadcrumb: 'Dashboard' }, component: DashboardComponent },
            { path: 'clinic', data: { breadcrumb: 'คลินิก' }, component: ClinicsComponent },
            { path: 'period', data: { breadcrumb: 'ช่วงเวลา' }, component: PeriodsComponent },
            { path: 'management', data: { breadcrumb: 'จัดการคิว' }, component: ManagementsComponent },
            { path: 'service', data: { breadcrumb: 'การให้บริการ' }, component: ServicesComponent },
            { path: 'slot', data: { breadcrumb: 'Slot' }, component: SlotsComponent },
            { path: 'user', data: { breadcrumb: 'ผู้ใช้' }, component: UsersComponent },
            { path: 'role', data: { breadcrumb: 'บทบาท' }, component: RolesComponent },
            { path: 'hospital', data: { breadcrumb: 'หน่วยบริการ' }, component: HospitalsComponent}
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class BackendRoutingModule { }
