import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackendRoutingModule } from './backend-routing.module';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { Router, RouterModule } from '@angular/router';
import { StyleClassModule } from 'primeng/styleclass';
import { RippleModule } from 'primeng/ripple';
import { AppConfigModule } from 'src/app/layout/config/config.module';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from "primeng/inputtext";
import { InputSwitchModule } from 'primeng/inputswitch';
import { TableModule } from 'primeng/table';

// component pages
import { HomeComponent} from './components/home/home.component';
import { SlotsComponent } from './components/slots/slots.component';
import { UsersComponent } from './components/users/users.component';
import { RolesComponent } from './components/roles/roles.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ClinicsComponent } from './components/clinics/clinics.component';
import { PeriodsComponent } from './components/periods/periods.component';
import { ManagementsComponent } from './components/managements/managements.component';
import { ServicesComponent } from './components/services/services.component';
import { HospitalsComponent } from './components/hospitals/hospitals.component';

@NgModule({
    imports: [
        CommonModule,
        BackendRoutingModule,
        ButtonModule,
        RouterModule,
        StyleClassModule,
        RippleModule,
        AppConfigModule,
        DialogModule,
        FormsModule,
        RadioButtonModule,
        InputTextModule,
        InputSwitchModule,
        TableModule
    ],
    declarations: [
        HomeComponent,
        SlotsComponent,
        UsersComponent,
        RolesComponent,
        DashboardComponent,
        ClinicsComponent,
        PeriodsComponent,
        ManagementsComponent,
        ServicesComponent,
        HospitalsComponent
    ]
})
export class BackendModule { 

    constructor(private router: Router) {}
}