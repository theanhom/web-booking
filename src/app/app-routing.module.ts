import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppLayoutComponent } from './layout/app.layout.component';

const routes: Routes = [
    { path: '', pathMatch:'full', redirectTo: 'auth/login' },
    {
        path: 'main', component: AppLayoutComponent,
        children: [
            { path: 'dashboards', loadChildren: () => import('./front-end/components/dashboards/dashboards.module').then(m => m.DashboardsModule) },
            { path: '', loadChildren: () => import('./front-end/components/apps/apps.module').then(m => m.AppsModule) },
            { path: 'profile', data: { breadcrumb: 'User Management' }, loadChildren: () => import('./front-end/components/profile/profile.module').then(m => m.ProfileModule) },
            { path: 'documentation', data: { breadcrumb: 'Documentation' }, loadChildren: () => import('./front-end/components/documentation/documentation.module').then(m => m.DocumentationModule) },


            // { path: 'apps', data: { breadcrumb: 'Apps' }, loadChildren: () => import('./front-end/components/apps/apps.module').then(m => m.AppsModule) }
        ]
    },
    // { path: 'auth', data: { breadcrumb: 'Auth' }, loadChildren: () => import('./front-end/components/auth/auth.module').then(m => m.AuthModule) },
    { path: 'auth', data: { breadcrumb: 'Authenticate' }, loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
    { path: 'frontend', loadChildren: () => import('./front-end/components/landing/landing.module').then(m => m.LandingModule) },
    { path: 'notfound', loadChildren: () => import('./front-end/components/notfound/notfound.module').then(m => m.NotfoundModule) },
    { path: 'backend',  component: AppLayoutComponent, loadChildren: () => import('./backend/backend.module').then(m => m.BackendModule) },
    //{ path: 'manage-slot', loadChildren: () => import('./front-end/components/apps/manage-slot/manage-slot.module').then(m => m.ManageSlotModule) },
    // { path: '**', redirectTo: '/notfound' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
