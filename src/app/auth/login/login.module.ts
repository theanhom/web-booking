import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {UserProfileService} from '../../shared/user-profiles.service'


import { LoginRoutingModule } from './login-routing.module';


import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { AppConfigModule } from 'src/app/layout/config/config.module';
import { RippleModule } from 'primeng/ripple';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';

import {LoginComponent} from './login.component';


@NgModule({
    imports: [
        CommonModule,FormsModule,ReactiveFormsModule,
        LoginRoutingModule,
        ButtonModule,
        InputTextModule,
        RippleModule,
        AppConfigModule,
        MessagesModule,MessageModule
    ],
    declarations: [LoginComponent],
    providers:[UserProfileService]
})
export class LoginModule { }
