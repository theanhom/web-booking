import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Message } from 'primeng/api';

import { LayoutService } from 'src/app/layout/service/app.layout.service';
import {UserProfileService} from '../../shared/user-profiles.service'

import { LoginService } from './login-service';

@Component({
    templateUrl: './login.component.html',
})
export class LoginComponent {
    jwtHelper: JwtHelperService = new JwtHelperService();
    public validateForm!: FormGroup;
    public passwordVisible: Boolean = false;

    // messages: Message[] | undefined;
    messages: any | undefined;
	userRole: string = '';

    constructor(
        public login: FormBuilder,
        private layoutService: LayoutService,
		private userProfileService: UserProfileService,		
        private router: Router,
        private loginService: LoginService
    ) {

    }

    ngOnInit(): void {
        this.validateForm = this.login.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required]],
            remember: [true],
        });
    }

    // alert message
    showMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },

            // { severity: 'success', summary: 'Success', detail: 'Message Content' },
            // { severity: 'info', summary: 'Info', detail: 'Message Content' },
            // { severity: 'warn', summary: 'Warning', detail: 'Message Content' },
            // { severity: 'error', summary: 'Error', detail: 'Message Content' }
        ];
    }

    clearMessages() {
        this.messages = [];
    }

    get filledInput(): boolean {
        return this.layoutService.config.inputStyle === 'filled';
    }

    loginBypass() {
        sessionStorage.setItem('token', 'token');
        sessionStorage.setItem('user_login', 'DK');
        this.router.navigate(['/main']);
    }

    async onSubmit() {
        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }

        if (this.validateForm.status == 'INVALID') {
            this.showMessages('error', 'Error', 'กรุณาตรวจสอบข้อมูลให้ถูกต้อง');

            return;
        }

        if (this.validateForm.status == 'VALID') {
            let { username, password } = this.validateForm.value;
            // console.log('login:',username,password);
            try {
                const response: any = await this.loginService.login(
                    username,
                    password
                );
                // console.log('user:',response);
                if (response.data) {
                    // set token
                    const token = response.data.results.accessToken;
                    // set user role
                    const userRole = response.data.results.info.role.role_id;
                    // set user login name
                    const userLoginName = response.data.results.info.name;
                    
                    // session storage token, user_role, userLoginName
                    sessionStorage.setItem('token', token);
                    sessionStorage.setItem('userRole',userRole);
                    sessionStorage.setItem('userLoginName', userLoginName);

                    // show message
                    this.showMessages(
                        'success',
                        'Success',
                        'เข้าสู่ระบบสำเร็จ'
                    );

                    const decoded = this.jwtHelper.decodeToken(token);
                    if(userRole == 1 || userRole == 2 || userRole == 3){
                        // backend page
                        this.router.navigate(['/backend']);
                      } else if(userRole.role_id == '4'){
                        // main page
                        this.router.navigate(['/main']);
                      } else {
                        // not found page
                        this.router.navigate(['/notfound']);
                      }
					
                } else {
                    this.showMessages(
                        'error',
                        'Error',
                        'ชื่อผู้ใช้งาน/รหัสผ่าน ไม่ถูกต้อง'
                    );
                }
            } catch (error: any) {
                this.showMessages(
                    'error',
                    'Error',
                    'cathc เกิดปัญหาในการ login'
                );
            }
        }
    }
}
