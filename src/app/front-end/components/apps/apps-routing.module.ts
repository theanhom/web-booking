import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [RouterModule.forChild([
        { path: '', data: { breadcrumb: 'Activites' }, loadChildren: () => import('./activities/activities.module').then(m => m.ActivitiesModule) },
        { path: 'hospital', data: { breadcrumb: 'Hospital' }, loadChildren: () => import('./hospital/hospital.module').then(m => m.HospitalModule) },
        { path: 'calendar', data: { breadcrumb: 'Calendar' }, loadChildren: () => import('./calendar/calendar.app.module').then(m => m.CalendarAppModule) },
        { path: 'list-booking', data: { breadcrumb: 'list-booking' }, loadChildren: () => import('./list-booking/list-booking.module').then(m => m.ListBookingModule) },
        { path: 'manage-booking', data: { breadcrumb: 'manage-booking' }, loadChildren: () => import('./manage-booking/manage-booking.module').then(m => m.ManageBookingModule) },
        { path: 'manage-clinic', data: { breadcrumb: 'manage-clinic' }, loadChildren: () => import('./manage-clinic/manage-clinic.module').then(m => m.ManageClinicModule) },
        { path: 'manage-period', data: { breadcrumb: 'manage-period' }, loadChildren: () => import('./manage-period/manage-period.module').then(m => m.ManagePeriodModule) },
        { path: 'manage-service', data: { breadcrumb: 'manage-service' }, loadChildren: () => import('./manage-service/manage-service.module').then(m => m.ManageServiceModule) },
        { path: 'manage-slot', data: { breadcrumb: 'manage-slot' }, loadChildren: () => import('./manage-slot/manage-slot.module').then(m => m.ManageSlotModule) },


        // { path: '**', redirectTo: '/notfound' }
    ])],
    exports: [RouterModule]
})
export class AppsRoutingModule { }
