import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageSlotRoutingModule } from './manage-slot-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ManageSlotRoutingModule
  ]
})
export class ManageSlotModule { }
