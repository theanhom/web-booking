import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageBookingRoutingModule } from './manage-booking-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ManageBookingRoutingModule
  ]
})
export class ManageBookingModule { }
