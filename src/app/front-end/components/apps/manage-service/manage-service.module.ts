import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageServiceRoutingModule } from './manage-service-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ManageServiceRoutingModule
  ]
})
export class ManageServiceModule { }
