import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListBookingRoutingModule } from './list-booking-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ListBookingRoutingModule
  ]
})
export class ListBookingModule { }
