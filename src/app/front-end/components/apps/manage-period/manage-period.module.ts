import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagePeriodRoutingModule } from './manage-period-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ManagePeriodRoutingModule
  ]
})
export class ManagePeriodModule { }
