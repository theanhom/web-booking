import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageClinicRoutingModule } from './manage-clinic-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ManageClinicRoutingModule
  ]
})
export class ManageClinicModule { }
