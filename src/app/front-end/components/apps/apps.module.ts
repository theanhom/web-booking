import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppsRoutingModule } from './apps-routing.module';
import { ManageSlotComponent } from './manage-slot/manage-slot.component';
import { ManageServiceComponent } from './manage-service/manage-service.component';
import { ManagePeriodComponent } from './manage-period/manage-period.component';
import { ManageClinicComponent } from './manage-clinic/manage-clinic.component';
import { ManageBookingComponent } from './manage-booking/manage-booking.component';
import { ListBookingComponent } from './list-booking/list-booking.component';

@NgModule({
  imports: [
    CommonModule,
    AppsRoutingModule
  ],
  declarations: [
    ManageSlotComponent,
    ManageServiceComponent,
    ManagePeriodComponent,
    ManageClinicComponent,
    ManageBookingComponent,
    ListBookingComponent
  ]
})
export class AppsModule { }
